package com.homekode.android.homeviii

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import java.io.File

class MainActivity : AppCompatActivity() {

    private val IMG_FROM_GALLERY = 0
    private val URL_COLORIZER = "https://api.deepai.org/api/colorizer"
    private val URL_INPAINTING = "https://api.deepai.org/api/inpainting"
    private var pathImg: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_choose.setOnClickListener{
            startActivityForResult(Intent(Intent.ACTION_PICK).setType("image/*"),IMG_FROM_GALLERY)
            changeVisible(false, img_after)
        }

        btn_color.setOnClickListener{
            updateImg(URL_COLORIZER)
        }

        btn_inpaint.setOnClickListener{
            updateImg(URL_INPAINTING)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK && requestCode == IMG_FROM_GALLERY && data != null) {
            pathImg =  FileUtils.getPath(applicationContext, data.data)
            Picasso.with(applicationContext)
                    .load(File(pathImg))
                    .into(img_before)
        }
    }

    private fun updateImg(baseUrl: String) {
        if( pathImg != null ) {
            changeVisible(true, loading)
            FuelManager.instance.baseHeaders = mapOf("api-key" to getString(R.string.api_key))
            Fuel.upload(baseUrl).source{ request, url -> File(pathImg)}
                    .name{"image"}
                    .responseString { request, response, result ->
                        when(result) {
                            is Result.Success -> {
                                showResult(Gson().fromJson(result.get(), ImgReturn::class.java))
                                Log.v("!!!", result.get())
                            }
                            is Result.Failure -> { Toast.makeText(applicationContext, result.get(), Toast.LENGTH_SHORT).show() }
                        }
                    }
        } else {
            Toast.makeText(applicationContext, getString(R.string.choose_image), Toast.LENGTH_SHORT).show()
        }
    }

    private fun showResult(updateImg: ImgReturn) {

        val url = updateImg.output_url
        if (url != null) {
            Picasso.with(applicationContext)
                    .load(url)
                    .into(img_after)
            changeVisible(false, loading)
            changeVisible(true, img_after)
        }
    }

    private fun changeVisible(visible: Boolean, view: View) {
        if(visible)
            view.visibility = View.VISIBLE
        else
            view.visibility = View.GONE
    }


    /*private fun getTarget(url: String): Target {
        return object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom) {

                Thread(Runnable {
                    val file = File(Environment.getExternalStorageDirectory().path + "/Pictures/" + "kek")
                    try {
                        file.createNewFile()
                        val fos = FileOutputStream(file)
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos)
                        fos.flush()
                        fos.close()
                    } catch (e: IOException) {
                        Log.e("IOException", e.localizedMessage)
                    }
                }).start()
            }

            override fun onBitmapFailed(errorDrawable: Drawable) {}
            override fun onPrepareLoad(placeHolderDrawable: Drawable) {}
        }
    }*/

}
